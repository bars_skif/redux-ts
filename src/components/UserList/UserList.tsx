import { FC, useEffect, useState } from "react"
import { useTypedSelector } from "../../hooks/useTypesSelector";
import { useDispatch } from 'react-redux'
import { fetchUsers } from "../../store/action-creator/user";
import { fetchGitUsers } from "../../store/action-creator/gitUser";


import cl from './UserList.module.css'



const UserList: FC = () => {
    const [nameSub, setNmeSub] = useState('subject')
    const { errorGit, loadingGit, usersGit } = useTypedSelector(state => state.gitUser)
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(fetchUsers())
        dispatch(fetchGitUsers(15, nameSub))
    }, [nameSub, dispatch])


    const newSerch = () => {
        setNmeSub(prev => prev = JSON.stringify(prompt()))
    }


    if (loadingGit) {
        return (
            <h1>Идет загрузка</h1>
        )
    }
    if (errorGit) {
        return (
            <>
                <h1>{errorGit}</h1>
            </>
        )
    }
    return (
        <>
            <div className={cl.wrap}>
                <div className={cl.header_block}>
                    serch
                </div>
                {usersGit.map(({ id, name }) => <div key={id}>{name}</div>)}
            </div>
        </>
    )

}


export default UserList