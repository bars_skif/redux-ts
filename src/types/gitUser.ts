export interface GitUserState {
    usersGit: any[];
    loadingGit: boolean;
    errorGit: string | null;
}

export enum UserGitActionTypes {
    FETCH_GIT_USER = 'FETCH_GIT_USER',
    FETCH_GIT_USERS_SUCCESS = 'FETCH_GIT_USERS_SUCCESS',
    FETCH_GIT_USERS_ERROR = 'FETCH_GIT_USERS_ERROR'
}

interface FetchUserAction {
    type: UserGitActionTypes.FETCH_GIT_USER
}
interface FetchUserSussesAction {
    type: UserGitActionTypes.FETCH_GIT_USERS_SUCCESS;
    payload: any[]
}

interface FetchUserErrorAction {
    type: UserGitActionTypes.FETCH_GIT_USERS_ERROR;
    payload: string
}

export type UseGitAction = FetchUserAction | FetchUserSussesAction | FetchUserErrorAction