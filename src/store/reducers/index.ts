import { combineReducers } from 'redux'
import { userGitReducer } from './gitUsersReducer'
import { userReducer } from './userReducer'



export const rootReducer = combineReducers({
    user: userReducer,
    gitUser: userGitReducer
})



export type RootState = ReturnType<typeof rootReducer>