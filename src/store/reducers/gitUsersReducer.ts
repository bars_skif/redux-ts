import { UseGitAction, UserGitActionTypes, GitUserState } from "../../types/gitUser"


const defaultState: GitUserState = {
    usersGit: [],
    loadingGit: false,
    errorGit: null,
}

export const userGitReducer = (state = defaultState, action: UseGitAction): GitUserState => {
    switch (action.type) {
        case UserGitActionTypes.FETCH_GIT_USER:
            return { loadingGit: true, errorGit: null, usersGit: [] }
        case UserGitActionTypes.FETCH_GIT_USERS_SUCCESS:
            return { loadingGit: false, errorGit: null, usersGit: action.payload }
        case UserGitActionTypes.FETCH_GIT_USERS_ERROR:
            return { loadingGit: false, errorGit: action.payload, usersGit: [] }
        default:
            return state
    }
}

