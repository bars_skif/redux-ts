import { Dispatch } from "redux";
import { UseGitAction, UserGitActionTypes } from "../../types/gitUser";
import axios from 'axios'


export const fetchGitUsers = (page: number | null, subject: string) => {
    return async (dispatch: Dispatch<UseGitAction>) => {
        try {
            dispatch({ type: UserGitActionTypes.FETCH_GIT_USER })
            const response = await axios.get(`https://api.github.com/search/repositories?q=${subject}&per_page=${page}`)
            dispatch({ type: UserGitActionTypes.FETCH_GIT_USERS_SUCCESS, payload: response.data.items })
        } catch (error) {
            dispatch({ type: UserGitActionTypes.FETCH_GIT_USERS_ERROR, payload: 'Ошибка при загрузке данных!' })
        }
    }
}